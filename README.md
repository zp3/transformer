# CS547 Project - Transformer

Requirements: numpy, pytorch, torchvision, pytorch_lightning, sentencepiece

- Run `python3 get_wmt2014.py` to prepare the WMT2014 dataset.
- Modify `common.py` for constants used.
- Run `python3 train.py` to train the model.
- Run `python3 test.py` to get the BLEU score.
- Run `python3 translate.py` to start a prompt for live translation.
