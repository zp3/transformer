import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

class NoamOpt(torch.optim.Adam):
    "Optim wrapper that implements rate."
    def __init__(self, params, size, factor, warmup, **kwargs):
        super(NoamOpt, self).__init__(params, **kwargs)
        self._step = 0
        self.size = size
        self.warmup = warmup
        self.factor = factor
        
    def step(self, closure=None):
        self._step += 1
        rate = self.factor * \
            (self.size ** (-0.5) *
            min(self._step ** (-0.5), self._step * self.warmup ** (-1.5)))
        for p in self.param_groups:
            p['lr'] = rate
        super().step(closure)
    
    def zero_grad(self, set_to_none=False):
        super().zero_grad(set_to_none)

class LabelSmoothing(nn.Module):
    def __init__(self, size, padding_idx, smoothing=0.0):
        super(LabelSmoothing, self).__init__()
        self.criterion = nn.KLDivLoss(reduction='sum')
        self.padding_idx = padding_idx
        self.confidence = 1.0 - smoothing
        self.smoothing = smoothing
        self.size = size
        
    def forward(self, x, target):
        assert x.size(1) == self.size
        true_dist = x.data.clone()
        true_dist.fill_(torch.div(self.smoothing, (self.size - 2)))
        true_dist.scatter_(1, target.data.unsqueeze(1), self.confidence)
        true_dist[:, self.padding_idx] = 0
        mask = torch.nonzero(target.data == self.padding_idx)
        if mask.dim() > 0:
            true_dist.index_fill_(0, mask.squeeze(), 0.0)
        return self.criterion(x, Variable(true_dist, requires_grad=False))
