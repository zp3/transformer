#%%
import os
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
import pytorch_lightning as pl
import codecs
import sentencepiece as spm

from utils import subsequent_mask


#%%
class SyntheticDataset(Dataset):
    def __init__(self, vocab, seq_len, length):
        super(SyntheticDataset, self).__init__()
        src = torch.from_numpy(np.random.randint(1, vocab, size=(length, seq_len)))
        src[:, 0] = 1
        self.src = src
        self.src_mask = (src != 0).unsqueeze(-2)
        self.tgt = src[:, :-1]
        self.tgt_y = src[:, 1:]
        self.tgt_mask = (self.tgt != 0).unsqueeze(-2)
        self.tgt_mask = self.tgt_mask & subsequent_mask(self.tgt.size(-1)).type_as(self.tgt_mask.data)
        self.ntokens = (self.tgt_y != 0).data.sum(axis=1)
    
    def __len__(self):
        return self.src.size(0)
    
    def __getitem__(self, idx):
        return (self.src[idx], self.src_mask[idx], self.tgt[idx], self.tgt_y[idx], self.tgt_mask[idx], self.ntokens[idx])

class SyntheticDataModule(pl.LightningDataModule):
    def __init__(self, batch_size=30, vocab=11, seq_len=10, length=(6000, 150, 300), num_workers=1):
        super().__init__()
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.vocab = vocab
        self.seq_len = seq_len
        self.train_len, self.val_len, self.test_len = length
    
    def setup(self, stage=None):
        self.train = SyntheticDataset(self.vocab, self.seq_len, self.train_len)
        self.val = SyntheticDataset(self.vocab, self.seq_len, self.val_len)
        self.test = SyntheticDataset(self.vocab, self.seq_len, self.test_len)
    
    def train_dataloader(self):
        return DataLoader(self.train, batch_size=self.batch_size, num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val, batch_size=self.batch_size, num_workers=self.num_workers)

    def test_dataloader(self):
        return DataLoader(self.test, batch_size=self.batch_size, num_workers=self.num_workers)

#%%
class WMT2014Dataset(Dataset):
    def __init__(self, sl_path, tl_path, sp, max_seq_len):
        super(WMT2014Dataset, self).__init__()
        self.sl_sentences, self.tl_sentences = [], []
        pad_idx, sos_idx, eos_idx = sp['<blank>'], sp['<s>'], sp['</s>']
        with codecs.open(sl_path, 'r', 'utf-8') as f:
            for l in f:
                tmp = l.split()
                assert len(tmp) > 0
                self.sl_sentences.append(torch.LongTensor(sp.piece_to_id(tmp)[:max_seq_len]))
        with codecs.open(tl_path, 'r', 'utf-8') as f:
            for l in f:
                tmp = l.split()
                assert len(tmp) > 0
                self.tl_sentences.append(torch.LongTensor(([sos_idx] + sp.piece_to_id(tmp) + [eos_idx])[:max_seq_len]))
        assert len(self.sl_sentences) == len(self.tl_sentences)
    
    def __len__(self):
        assert len(self.sl_sentences) == len(self.tl_sentences)
        return len(self.sl_sentences)
    
    def __getitem__(self, idx):
        return (self.sl_sentences[idx], self.tl_sentences[idx])

#%%
def WMT2014_collate(pad_idx):
    def inner(batch):
        src = pad_sequence([t[0] for t in batch], batch_first=True, padding_value=pad_idx)
        tmptgt = pad_sequence([t[1] for t in batch], batch_first=True, padding_value=pad_idx)
        src_mask = (src != pad_idx).unsqueeze(-2)
        tgt = tmptgt[:, :-1]
        tgt_y = tmptgt[:, 1:]
        tgt_mask = (tgt != pad_idx).unsqueeze(-2)
        tgt_mask = tgt_mask & subsequent_mask(tgt.size(-1)).type_as(tgt_mask.data)
        ntokens = (tgt_y != pad_idx).data.sum(axis=1)
        return (src, src_mask, tmptgt, tgt_mask, ntokens)
    return inner

#%%
class WMT2014DataModule(pl.LightningDataModule):
    def __init__(self, model_path, train_path, val_path, test_path, sl, tl, max_seq_len=2048, batch_size=128, num_workers=1):
        super().__init__()
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.sp = spm.SentencePieceProcessor(model_file=model_path)
        self.pad_idx, self.sos_idx, self.eos_idx = self.sp['<blank>'], self.sp['<s>'], self.sp['</s>']
        self.vocabSize = self.sp.GetPieceSize()
        self.train_path, self.val_path, self.test_path = train_path, val_path, test_path
        self.sl, self.tl = sl, tl
        self.max_seq_len = max_seq_len
        self.train, self.val, self.test = None, None, None
        self.initialized = False
    
    def setup(self, stage=None):
        if not self.initialized:
            print('Initializing data module')
            if self.train_path:
                self.train = WMT2014Dataset(self.train_path + '.' + self.sl, self.train_path + '.' + self.tl, self.sp, self.max_seq_len)
            if self.val_path:
                self.val = WMT2014Dataset(self.val_path + '.' + self.sl, self.val_path + '.' + self.tl, self.sp, self.max_seq_len)
            if self.test_path:
                self.test = WMT2014Dataset(self.test_path + '.' + self.sl, self.test_path + '.' + self.tl, self.sp, self.max_seq_len)
            self.initialized = True
        print('Data module initialized')
    
    def train_dataloader(self):
        return DataLoader(
            self.train, 
            collate_fn=WMT2014_collate(self.pad_idx), 
            batch_size=self.batch_size, 
            num_workers=self.num_workers,
            shuffle=True,
            pin_memory=True,
            drop_last=True
        )

    def val_dataloader(self):
        return DataLoader(
            self.val, 
            collate_fn=WMT2014_collate(self.pad_idx), 
            batch_size=self.batch_size, 
            num_workers=self.num_workers,
            pin_memory=True,
            drop_last=True
        )

    def test_dataloader(self):
        return DataLoader(
            self.test, 
            collate_fn=WMT2014_collate(self.pad_idx), 
            batch_size=self.batch_size, 
            num_workers=self.num_workers,
            pin_memory=True,
            drop_last=True
        )
