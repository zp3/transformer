import os
from torchvision.datasets.utils import download_file_from_google_drive, extract_archive

if __name__ == '__main__':
    TEST_ID = '1PcMHcW8NWVq4544IMuwNQ4REWYKO0mYj'
    FULL_ID = '1Spk9gPIdB3wLLosAh652T6JJdmTM-qtq'

    if not os.path.exists('data'):
        os.makedirs('data')
    if not os.path.exists('data/wmt14'):
        os.makedirs('data/wmt14')

    download_file_from_google_drive(
        file_id=FULL_ID,
        root='./data/',
        filename='tmp.tar.gz'
    )
    extract_archive('./data/tmp.tar.gz', './data/wmt14/', remove_finished=True)

