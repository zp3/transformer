import torch
import torch.nn as nn
import torch.nn.functional as F
import pytorch_lightning as pl

from model import Transformer
from optimizer import NoamOpt, LabelSmoothing
from utils import greedy_decode, beam_search

class TransformerModule(pl.LightningModule):
    def __init__(self, src_vocab, tgt_vocab, pad_idx, sos_idx, eos_idx,
                 enc_num_layers=6, dec_num_layers=6,
                 d_model=512, d_ff=2048, h=8, max_seq_len=2048, 
                 smoothing=0.1, factor=1, warmup=400, lr=0.0007, beta1=0.9, beta2=0.98,
                 dropout=0.1, eps=1e-5):
        super(TransformerModule, self).__init__()
        self.factor = factor
        self.warmup = warmup
        self.lr = lr
        self.betas = (beta1, beta2)
        self.eps = eps

        self.pad_idx, self.sos_idx, self.eos_idx = pad_idx, sos_idx, eos_idx
        self.max_seq_len = max_seq_len
        self.model = Transformer(src_vocab, tgt_vocab, enc_num_layers, dec_num_layers, 
                                 d_model, h, d_ff, max_seq_len, dropout, eps)
        self.criterion = LabelSmoothing(tgt_vocab, pad_idx, smoothing)

        for p in self.model.parameters():
            if p.dim() > 1:
                nn.init.xavier_uniform_(p)

    def forward(self, src, src_mask):
        return beam_search(self.model, src, src_mask, self.max_seq_len, self.sos_idx, self.eos_idx)
    
    def training_step(self, batch, batch_idx):
        loss = self.shared_step(batch)
        self.trainer.train_loop.running_loss.append(loss)
        self.log('train_loss', loss)
        return loss
    
    def validation_step(self, batch, batch_idx):
        self.log('val_loss', self.shared_step(batch))
    
    def test_step(self, batch, batch_idx):
        self.log('test_loss', self.shared_step(batch))
    
    def shared_step(self, batch):
        src, src_mask, tgt, tgt_mask, ntokens = batch
        out = self.model(src, tgt[:, :-1], src_mask, tgt_mask)
        return torch.div(self.criterion(out.contiguous().view(-1, out.size(-1)), tgt[:, 1:].contiguous().view(-1)),
                         ntokens.sum())

    def configure_optimizers(self):
        optimizer = NoamOpt(
            self.parameters(), 
            size=self.model.d_model,
            factor=self.factor,
            warmup=self.warmup,
            lr=self.lr,
            betas=self.betas,
            eps=self.eps
        )
        return optimizer
    
