#%%
import numpy as np
import torch

#%%
def subsequent_mask(size):
    mask = np.triu(np.ones((1, size, size)), k=1).astype('uint8')
    return torch.from_numpy(mask) == 0

#%%
def greedy_decode(model, src, src_mask, max_len, start_symbol, stop_symbol):
    memory = model.encode(src, src_mask)
    ys = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)
    for i in range(1, max_len):
        out = model.decode(memory, src_mask, ys, subsequent_mask(ys.size(1)).type_as(src.data))

        prob = model.proj(out[:, -1])
        _, next_word = torch.max(prob, dim = 1)
        next_word = next_word.data[0]
        ys = torch.cat([ys, torch.ones(1, 1).fill_(next_word).type_as(src.data)], dim=1)
        if next_word.item() == stop_symbol:
            break

    return ys

#%%
def beam_search(model, src, src_mask, max_len, start_symbol, stop_symbol, k=5, alpha=0.7):
    memory = model.encode(src, src_mask)
    ys = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)
    out = model.decode(memory, src_mask, ys, subsequent_mask(ys.size(1)).type_as(src.data))
    log_scores, idx = model.proj(out[:, -1]).data.topk(k)

    outputs = torch.zeros(k, max_len).type_as(src.data)
    outputs[:, 0] = start_symbol
    outputs[:, 1] = idx[0]

    memorys = torch.zeros(k, memory.size(-2), memory.size(-1))
    memorys[:, :] = memory[0]

    for i in range(2, max_len):
        out = model.decode(memory, src_mask, outputs[:, :i], subsequent_mask(i).type_as(src.data))

        log_probs, idx = model.proj(out[:, -1]).data.topk(k)
        log_probs = log_probs + log_scores.transpose(0, 1)
        k_probs, k_ix = log_probs.view(-1).topk(k)

        assert k != 0
        row = k_ix // k
        col = k_ix % k

        outputs[:, :i] = outputs[row, :i]
        outputs[:, i] = idx[row, col].type_as(src.data)

        log_scores = k_probs.unsqueeze(0)

        if (((outputs == stop_symbol).sum(axis=1)) > 0).sum() == k:
            break

    _, sentence_len = (((outputs == stop_symbol).cumsum(axis=1) == 1) & (outputs == stop_symbol)).max(axis=1)
    sentence_len[sentence_len == 0] = max_len - 1
    sentence_len += 1

    _, ind = torch.max(log_scores * (torch.div(1, (sentence_len.type_as(log_scores) ** alpha))), axis=1)

    return outputs[ind, :sentence_len[ind]]

# %%
