#%%
import os
import codecs
import pickle as pkl
from tqdm import tqdm
from argparse import ArgumentParser

import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader

import sentencepiece as spm
import sacrebleu

from module import TransformerModule
from utils import beam_search
from common import *

#%%
def main(hparams):

    sp = spm.SentencePieceProcessor(model_file=hparams.sp_model)
    pad_idx, sos_idx, eos_idx = sp['<blank>'], sp['<s>'], sp['</s>']

    V = sp.GetPieceSize()
    model = TransformerModule.load_from_checkpoint(
        hparams.ckpt, 
        src_vocab=V, tgt_vocab=V, 
        pad_idx=pad_idx, sos_idx=sos_idx, eos_idx=eos_idx, 
        enc_num_layers=hparams.enc_num_layers, dec_num_layers=hparams.dec_num_layers,
        d_model=hparams.d_model, d_ff=hparams.d_ff,
        h=hparams.num_heads, max_seq_len=hparams.max_seq_len,
        dropout=0.0, eps=1e-5
    ).to(hparams.device)
    
    model.eval()

    total = 0
    with codecs.open(hparams.path + '.' + hparams.tl, 'r', 'utf-8') as f:
        for l in f:
            total  += 1
    
    refs = []
    with codecs.open(hparams.path + '.' + hparams.tl, 'r', 'utf-8') as f:
        for l in tqdm(f, total=total):
            refs.append(sp.decode(l.split()))
    
    mts = []
    with codecs.open(hparams.path + '.' + hparams.sl, 'r', 'utf-8') as f:
        for l in tqdm(f, total=total):
            src = Variable(torch.LongTensor([sp.piece_to_id(l.split())[:hparams.max_seq_len]])).to(hparams.device)
            src_mask = Variable((src != pad_idx).unsqueeze(-2)).to(hparams.device)
            mts.append(sp.decode(
                beam_search(model.model, src, src_mask, hparams.max_seq_len, sp['<s>'], sp['</s>'], 10)[0].cpu().numpy().tolist()
            ))
    
    bleu = sacrebleu.corpus_bleu(mts, [refs])
    print(bleu.score)

    with open(hparams.save, 'wb') as f:
        pkl.dump((mts, refs), f)


# %%
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--device', type=str, default="cpu")
    parser.add_argument('--ckpt', type=str, default=os.path.join(EXP_DIR, 'last.ckpt'))
    parser.add_argument('--save', type=str, default='test_result.pkl')
    parser.add_argument('--sp_model', type=str, default=SP_MODEL)
    parser.add_argument('--path', type=str, default=TEST_PATH)
    parser.add_argument('--sl', type=str, default=SRC_LANG)
    parser.add_argument('--tl', type=str, default=TGT_LANG)
    parser.add_argument('--max_seq_len', type=int, default=MAX_SEQ_LEN)
    
    parser.add_argument('--enc_num_layers', type=int, default=ENC_NUM_LAYERS)
    parser.add_argument('--dec_num_layers', type=int, default=DEC_NUM_LAYERS)
    parser.add_argument('--d_model', type=int, default=D_MODEL)
    parser.add_argument('--d_ff', type=int, default=D_FF)
    parser.add_argument('--num_heads', type=int, default=NUM_HEADS)
    args = parser.parse_args()

    main(args)
    
