#%%
import os
from argparse import ArgumentParser

import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor

import sentencepiece as spm

from module import TransformerModule
from dataloader import SyntheticDataModule, WMT2014DataModule
from common import *

#%%
def main(hparams):

    data = WMT2014DataModule(
        hparams.sp_model,
        hparams.train_path,
        hparams.val_path,
        None,
        hparams.sl, hparams.tl,
        max_seq_len=hparams.max_seq_len,
        batch_size=hparams.batch_size,
        num_workers=hparams.num_workers
    )

    V = data.vocabSize
    sp = data.sp
    model = TransformerModule(
        src_vocab=V, tgt_vocab=V, 
        pad_idx=sp['<blank>'], sos_idx=sp['<s>'], eos_idx=sp['</s>'],
        enc_num_layers=hparams.enc_num_layers, dec_num_layers=hparams.dec_num_layers,
        d_model=hparams.d_model, d_ff=hparams.d_ff,
        h=hparams.num_heads, max_seq_len=hparams.max_seq_len,
        smoothing=hparams.smoothing, factor=hparams.noam_factor,
        warmup=hparams.noam_warmup, lr=hparams.noam_lr,
        beta1=hparams.noam_beta1, beta2=hparams.noam_beta2,
        dropout=hparams.dropout, eps=hparams.eps
    )

    if not os.path.exists(hparams.exp_dir):
        os.makedirs(hparams.exp_dir)

    checkpoint_callback = ModelCheckpoint(
        dirpath=hparams.exp_dir,
        monitor='val_loss',
        mode='min',
        filename='transformer-{epoch:d}-{step:d}-{train_loss:.2f}-{val_loss:.2f}',
        save_last=True
    )
    # lr_monitor = LearningRateMonitor(logging_interval='step')
    logger = TensorBoardLogger('lightning_logs', name='Transformer', default_hp_metric=False)
    trainer = pl.Trainer(
        gpus=hparams.gpus,
        accelerator=hparams.accelerator,
        precision=hparams.precision,
        max_epochs=hparams.epoch,
        resume_from_checkpoint=hparams.ckpt,
        track_grad_norm=2,
        gradient_clip_val=10.0,
        accumulate_grad_batches=hparams.accumulate_grad_batches,
        val_check_interval=0.1,
        callbacks=[checkpoint_callback],
        progress_bar_refresh_rate=1,
        logger=logger,
        log_every_n_steps=1,
        log_gpu_memory='all',
    )
    trainer.logger.log_hyperparams(hparams)
    trainer.fit(model, datamodule=data)


# %%
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--exp_dir', type=str, default=EXP_DIR)
    parser.add_argument('--gpus', type=int, default=None)
    parser.add_argument('--accelerator', type=str, default=None)
    parser.add_argument('--precision', type=int, default=32)
    parser.add_argument('--epoch', type=int, default=32)
    parser.add_argument('--ckpt', type=str, default=None)
    parser.add_argument('--sp_model', type=str, default=SP_MODEL)
    parser.add_argument('--train_path', type=str, default=TRAIN_PATH)
    parser.add_argument('--val_path', type=str, default=VAL_PATH)
    parser.add_argument('--sl', type=str, default=SRC_LANG)
    parser.add_argument('--tl', type=str, default=TGT_LANG)

    parser.add_argument('--max_seq_len', type=int, default=MAX_SEQ_LEN)
    parser.add_argument('--batch_size', type=int, default=16)
    parser.add_argument('--accumulate_grad_batches', type=int, default=1)
    parser.add_argument('--num_workers', type=int, default=0)
    
    parser.add_argument('--enc_num_layers', type=int, default=ENC_NUM_LAYERS)
    parser.add_argument('--dec_num_layers', type=int, default=DEC_NUM_LAYERS)
    parser.add_argument('--d_model', type=int, default=D_MODEL)
    parser.add_argument('--d_ff', type=int, default=D_FF)
    parser.add_argument('--num_heads', type=int, default=NUM_HEADS)
    parser.add_argument('--smoothing', type=float, default=0.1)
    parser.add_argument('--dropout', type=float, default=0.1)
    parser.add_argument('--eps', type=float, default=1e-9)
    parser.add_argument('--noam_factor', type=float, default=1)
    parser.add_argument('--noam_warmup', type=float, default=400)
    parser.add_argument('--noam_lr', type=float, default=0.0007)
    parser.add_argument('--noam_beta1', type=float, default=0.9)
    parser.add_argument('--noam_beta2', type=float, default=0.98)
    args = parser.parse_args()

    main(args)
    