EXP_DIR = 'exp_wmt14/'

SP_MODEL = 'data/wmt14/wmtende.model'
TRAIN_PATH = 'data/wmt14/train_clean'
VAL_PATH = 'data/wmt14/valid'
TEST_PATH = 'data/wmt14/test'
SRC_LANG = 'en'
TGT_LANG = 'de'
MAX_SEQ_LEN = 200

ENC_NUM_LAYERS = 6
DEC_NUM_LAYERS = 6
D_MODEL = 512
D_FF = 2048
NUM_HEADS = 8
