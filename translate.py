#%%
import os
import codecs
import pickle as pkl
from tqdm import tqdm
from argparse import ArgumentParser

import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader

import sentencepiece as spm
import sacrebleu

from module import TransformerModule
from utils import beam_search
from common import *

#%%
def main(hparams):

    sp = spm.SentencePieceProcessor(model_file=hparams.sp_model)
    pad_idx, sos_idx, eos_idx = sp['<blank>'], sp['<s>'], sp['</s>']

    V = sp.GetPieceSize()
    model = TransformerModule.load_from_checkpoint(
        hparams.ckpt, 
        src_vocab=V, tgt_vocab=V, 
        pad_idx=pad_idx, sos_idx=sos_idx, eos_idx=eos_idx, 
        enc_num_layers=hparams.enc_num_layers, dec_num_layers=hparams.dec_num_layers,
        d_model=hparams.d_model, d_ff=hparams.d_ff,
        h=hparams.num_heads, max_seq_len=hparams.max_seq_len,
        dropout=0.0, eps=1e-5
    )
    
    model.eval()

    try:
        while True:
            _in = input(">>> ")
            if not _in:
                break
            # print('Input:', _in)
            src = Variable(torch.LongTensor([sp.encode(_in)[:hparams.max_seq_len]]))
            src_mask = Variable((src != pad_idx).unsqueeze(-2))
            print('Encoded:', src)
            result = beam_search(model.model, src, src_mask, hparams.max_seq_len, sp['<s>'], sp['</s>'], 10)[0].numpy().tolist()
            print('Beam:', result)
            print('Result:', sp.decode(result))
            print()
    except KeyboardInterrupt as e:
        pass
    except EOFError as e:
        pass
    except Exception as e:
        print(f"Error: {e}")
        exit(1)

    print("\nExiting...")


# %%
if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--ckpt', type=str, default=os.path.join(EXP_DIR, 'last.ckpt'))
    parser.add_argument('--sp_model', type=str, default=SP_MODEL)
    parser.add_argument('--max_seq_len', type=int, default=MAX_SEQ_LEN)
    
    parser.add_argument('--enc_num_layers', type=int, default=ENC_NUM_LAYERS)
    parser.add_argument('--dec_num_layers', type=int, default=DEC_NUM_LAYERS)
    parser.add_argument('--d_model', type=int, default=D_MODEL)
    parser.add_argument('--d_ff', type=int, default=D_FF)
    parser.add_argument('--num_heads', type=int, default=NUM_HEADS)
    args = parser.parse_args()

    main(args)
    
