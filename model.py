import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from layers import PositionalEncoding, Embeddings, Encoder, Decoder

class Transformer(nn.Module):
    def __init__(self, src_vocab, tgt_vocab, enc_num_layers=6, dec_num_layers=6,
                 d_model=512, h=8, d_ff=2048, max_seq_len=2048, dropout=0.1, eps=1e-5):
        super(Transformer, self).__init__()
        self.d_model = d_model
        self.src_embed = nn.Sequential(Embeddings(d_model, src_vocab), PositionalEncoding(d_model, dropout, max_seq_len))
        self.tgt_embed = nn.Sequential(Embeddings(d_model, tgt_vocab), PositionalEncoding(d_model, dropout, max_seq_len))
        self.encoder = Encoder(enc_num_layers, d_model, h, d_ff, dropout, eps)
        self.decoder = Decoder(dec_num_layers, d_model, h, d_ff, dropout, eps)
        self.proj = nn.Sequential(nn.Linear(d_model, tgt_vocab), nn.LogSoftmax(dim=-1))

    @staticmethod
    def subsequent_mask(size):
        mask = np.triu(np.ones((1, size, size)), k=1).astype('uint8')
        return torch.from_numpy(mask) == 0
        
    def forward(self, src, tgt, src_mask, tgt_mask):
        return self.proj(self.decode(self.encode(src, src_mask), src_mask, tgt, tgt_mask))
    
    def encode(self, src, src_mask):
        return self.encoder(self.src_embed(src), src_mask)
    
    def decode(self, memory, src_mask, tgt, tgt_mask):
        return self.decoder(self.tgt_embed(tgt), memory, src_mask, tgt_mask)

    def project(self, x):
        return self.proj(x)
