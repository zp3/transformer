import math
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

class Embeddings(nn.Module):
    def __init__(self, d_model, vocab):
        super(Embeddings, self).__init__()
        self.emb = nn.Embedding(vocab, d_model)
        self.d_model = d_model

    def forward(self, x):
        return self.emb(x) * math.sqrt(self.d_model)

class PositionalEncoding(nn.Module):
    "Implement the PE function."
    def __init__(self, d_model, dropout, max_seq_len=2048):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        
        pe = torch.zeros(max_seq_len, d_model)
        position = torch.arange(0, max_seq_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * -(torch.div(math.log(10000.0), d_model)))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        self.register_buffer("pe", pe)
        
    def forward(self, x):
        x = x + Variable(self.pe[:, :x.size(1)], requires_grad=False)
        return self.dropout(x)

class AttentionHead(nn.Module):
    def __init__(self, d_model=512, d_k=64, d_v=64):
        super(AttentionHead, self).__init__()
        self.sq_d_k = d_k ** 0.5
        self.linear_q = nn.Linear(d_model, d_k)
        self.linear_k = nn.Linear(d_model, d_k)
        self.linear_v = nn.Linear(d_model, d_v)

    def forward(self, query, key, value, mask=None):
        Q, K, V = self.linear_q(query), self.linear_k(key), self.linear_v(value)
        out = torch.div(torch.matmul(Q, K.transpose(-2, -1)), self.sq_d_k)
        if mask is not None:
            out = out.masked_fill(mask == 0, -np.inf)
        out = torch.softmax(out, dim=-1)
        return torch.matmul(out, V)

class MultiHeadedAttention(nn.Module):
    def __init__(self, d_model=512, d_k=64, d_v=64, h=8):
        super(MultiHeadedAttention, self).__init__()
        self.heads = nn.ModuleList([AttentionHead(d_model, d_k, d_v) for _ in range(h)])
        self.linear = nn.Linear(h * d_v, d_model)
        
    def forward(self, query, key, value, mask=None):
        return self.linear(torch.cat([head(query, key, value, mask) for head in self.heads], dim=-1))

class PositionwiseFeedForward(nn.Module):
    def __init__(self, d_model, d_ff, dropout=0.1):
        super(PositionwiseFeedForward, self).__init__()
        self.layers = nn.Sequential(nn.Linear(d_model, d_ff), nn.ReLU(), nn.Dropout(dropout), nn.Linear(d_ff, d_model))

    def forward(self, x):
        return self.layers(x)

class Residual(nn.Module):
    def __init__(self, d_model=512, dropout=0.1, eps=1e-5):
        super(Residual, self).__init__()
        self.norm = nn.LayerNorm(d_model, eps=eps)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, sublayer):
        return x + self.dropout(sublayer(self.norm(x)))

class Encoder(nn.Module):
    def __init__(self, num_layers=6, d_model=512, h=8, d_ff=2048, dropout=0.1, eps=1e-5):
        super(Encoder, self).__init__()
        self.layers = nn.ModuleList([EncoderLayer(d_model, h, d_ff, dropout) for _ in range(num_layers)])
        self.norm = nn.LayerNorm(d_model)
        
    def forward(self, src, mask):
        for layer in self.layers:
            src = layer(src, mask)
        return self.norm(src)

class EncoderLayer(nn.Module):
    def __init__(self, d_model=512, h=8, d_ff=2048, dropout=0.1, eps=1e-5):
        super().__init__()
        assert h != 0
        d_k = d_v = d_model // h
        self.self_attn = MultiHeadedAttention(d_model, d_k, d_v, h)
        self.feedforward = PositionwiseFeedForward(d_model, d_ff, dropout)
        self.residual1 = Residual(d_model, dropout, eps)
        self.residual2 = Residual(d_model, dropout, eps)

    def forward(self, src, src_mask = None):
        x = self.residual1(src, lambda x: self.self_attn(x, x, x, src_mask))
        return self.residual2(x, self.feedforward)

class Decoder(nn.Module):
    def __init__(self, num_layers=6, d_model=512, h=8, d_ff=2048, dropout=0.1, eps=1e-5):
        super(Decoder, self).__init__()
        self.layers = nn.ModuleList([DecoderLayer(d_model, h, d_ff, dropout) for _ in range(num_layers)])
        self.norm = nn.LayerNorm(d_model)
        
    def forward(self, tgt, memory, src_mask, tgt_mask):
        for layer in self.layers:
            tgt = layer(tgt, memory, src_mask, tgt_mask)
        return self.norm(tgt)

class DecoderLayer(nn.Module):
    def __init__(self, d_model=512, h=8, d_ff=2048, dropout=0.1, eps=1e-5):
        super().__init__()
        assert h != 0
        d_k = d_v = d_model // h
        self.size = d_model
        self.self_attn = MultiHeadedAttention(d_model, d_k, d_v, h)
        self.enc_attn = MultiHeadedAttention(d_model, d_k, d_v, h)
        self.feedforward = PositionwiseFeedForward(d_model, d_ff, dropout)
        self.residual1 = Residual(d_model, dropout, eps)
        self.residual2 = Residual(d_model, dropout, eps)
        self.residual3 = Residual(d_model, dropout, eps)

    def forward(self, tgt, memory, memory_mask, tgt_mask):
        x = self.residual1(tgt, lambda x: self.self_attn(x, x, x, tgt_mask))
        x = self.residual2(x, lambda x: self.enc_attn(x, memory, memory, memory_mask))
        return self.residual3(x, self.feedforward)
        